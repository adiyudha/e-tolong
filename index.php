<?php 


?>


<html>
<head>
   <title>Beranda E-Tolong</title>

    <!-- BOOTSTRAP -->
   <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
   <link rel="stylesheet" href="css/custom_style.css">
</head>
<body>
    <!-- Navbar -->
    <nav class="py-2 navbar navbar-expand-lg fixed-top">
        <div class="container">
            <a class="navbar-brand" href="#">
                <!-- <img src="images/icon.png" alt="logo" class="logo pb-1"> -->
                E-Tolong
            </a>
            <button class="navbar-toggler navbar-dark" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto ">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index.php">Beranda</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Request</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Pantau</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Admin</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- END Navbar -->

    <!-- Hero Section -->
    <section id="hero">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1>Beritahu Kami Jika Butuh Bantuan</h1>
                    <h4>Mari Ulurkan Tangan Kepada Yang Membutuhkan</h4>
                    <button type="button" class="btn btn-light" onclick="location.href='voting.php'" ><strong>Request</strong></button>
                </div>
                <div class="col-5 img-col" id="ilustrasi">
                    <img src="assets/images/help.png" alt="Hero Image" class="float-end">
                </div>
            </div>
        </div>
    </section>
    <div>
        <img src="assets/images/wave1.png" alt="" class="wave">
    </div>
    <!-- END Hero Section -->

    <!-- Kades Section -->
    <section id="kades">
        <div class="container">
            <div class="deskripsi text-center">
                <h2>Apa itu E-Tolong ?</h2>
                <p>E-Tolong adalah Web Layanan untuk penderita positif
                    COVID-19 yang digunakan untuk melakukan <br>
                    bantuan kepada kepala desa setempat</p>
            </div>

            <div class="deskripsi text-center">
                <h2>Bagaimana cara menggunakannya ?</h2>
            </div>

            <!-- CARDS -->
            <div class="card mb-3 border-purple border-5 rounded-3">
                <img src="assets/images/card1.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h2 class="card-title">Isi Formulir Pada Menu Request</h2>
                    <p class="card-text">Pastikan kolom isian diisi dengan data yang benar.</p>
                </div>
            </div>

            <div class="card mb-3 border-purple border-5 rounded-3">
                <img src="assets/images/pantau-pesanan.png" class="card-img-top" alt="...">
                <div class="card-body">
                    <h2 class="card-title">Pantau Status Bantuan Pada Menu Pantau</h2>
                    <p class="card-text">Lihat proses pengantaran pada bantuanmu.</p>
                </div>
            </div>

            <div class="card mb-3 border-purple border-5 rounded-3">
                <img src="assets/images/antar.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h2 class="card-title">Bantuan diantarkan</h2>
                    <p class="card-text">Pastikan bantuan yang diantarkan sesuai dengan request.</p>
                </div>
            </div>
    </section>

    <!-- END Kades Section -->

    <!-- Profile Developer -->
    <div class="text-center p-3 footer-bawah">
        <h5>Developed by</h5>
        <p><small> Mohammad Adiyudha Wisnu Wardana </small> </p>
    </div>
    <!-- END Profile Developer -->

    <!-- Javascript -->
    <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.js"></script>
</body>
</html>